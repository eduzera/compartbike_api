$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "compartbike_api/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rivibike_api"
  s.version     = CompartbikeApi::VERSION
  s.authors     = ["Eduardo Zaghi", "Marco Singer", "Stefano Diem Benatti"]
  s.email       = ["eduardo@loldesign.com.br", "marco@loldesign.com.br","stefano@heavenstudio.com.br"]
  s.homepage    = "http://loldesign.com.br"
  s.summary     = "Api for board integration with totem."
  s.description = "Receive or Send Action for totem and send commands to board."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.2.8"
  s.add_dependency "compartbike_board", "~> 0.2.2"
  s.add_dependency "state_machine", "~> 1.1.2"
  s.add_dependency "resque", "~> 1.23"
  s.add_dependency "time_diff", "~> 0.3.0"
  s.add_dependency "delorean", "~> 2.1.0"
  s.add_dependency "mongoid", "~> 3.0.13"
  s.add_dependency "foreman", "~> 0.60.2"

  s.add_development_dependency "sqlite3"
end
