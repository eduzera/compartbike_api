require_dependency "compartbike_api/application_controller"

module CompartbikeApi
  class SlotsController < ApplicationController
    respond_to :json, :html
    before_filter :get_slot, except: :index

    def new
      @slot = Slot.new
    end

    def create
      @slot = Slot.new(params[:slot])
      if @slot.save
        redirect_to action: :index
      else
        render :new
      end
    end

    def index
      @slots = CompartbikeApi::Slot.all.sort_by(&:name)
      respond_with(@slots)
    end

    def show
      respond_with(@slot)
    end

    def destroy
      @slot = Slot.find(params[:id])
      @slot.destroy
      redirect_to action: :index
    end

    def release
      @release_manager = ReleaseManager.new(@slot, params[:cpf], params[:role])
      if @release_manager.create_release
        head :ok
      else
        @release_manager.create_invalid_rent
        head :error
      end
    end

    def reset
      @slot.reset!

      render json: "Reset Command sent to slot #{@slot.name}"
    end

    def get_slot
      @slot = CompartbikeApi::SlotManager.find(params[:id])
    end
  end
end
