require_dependency "compartbike_api/application_controller"

module CompartbikeApi
  class CyclistsController < ApplicationController
    respond_to :json

    def show
      manager = CyclistManager.new(params[:cpf])
      render json: manager.json
    end
  end
end