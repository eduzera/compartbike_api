require_dependency "compartbike_api/application_controller"

module CompartbikeApi
  class ReleasesController < ApplicationController
    respond_to :json, :html

    def index
      @releases = Release.all
      respond_with(@releases)
    end

    def not_syncronized
      @releases = Release.status_error_sync
      respond_with(@releases)
    end
  end
end