require_dependency "compartbike_api/application_controller"

module CompartbikeApi
  class MechanicalsController < ApplicationController
    respond_to :json

    def show
      manager = MechanicalManager.new(params[:cpf])
      render json: manager.json
    end
  end
end