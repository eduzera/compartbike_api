module CompartbikeApi
  class RemoteStation < ActiveResource::Base
    self.site         = CompartbikeApi.server_config[:path]
    self.format       = :json
    self.user         = CompartbikeApi.server_config[:login]
    self.password     = CompartbikeApi.server_config[:password]
    self.element_name = :station

    schema { string :id, :name, :oppended }

    def self.working?
      CompartbikeApi::RemoteStation.get(:verify_working, station_number: CompartbikeApi.server_config[:station_number])
    end
  end
end