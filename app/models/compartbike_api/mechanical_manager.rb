module CompartbikeApi
  class MechanicalManager

    def initialize(cpf)
      @user = CompartbikeApi::RemoteMechanical.find(cpf)

    rescue ActiveResource::ResourceNotFound
    end

    def json
      return not_found unless @user

      {
        cpf: @user.cpf,
        login: @user.login,
        type: @user.type,
        valid: true
      }
    end

    private

    def not_found
      {
        valid: false,
        reason: 'not_found'
      }
    end
  end
end