require 'resque-retry'

module CompartbikeApi
  class RemoteRental < ActiveResource::Base
    extend Resque::Plugins::Retry

    self.site         = CompartbikeApi.server_config[:path] + '/api/v1'
    self.format       = :json
    self.user         = CompartbikeApi.server_config[:login]
    self.password     = CompartbikeApi.server_config[:password]
    self.element_name = :rental

    schema { string :id, :rfid, :station_number, :cpf, :position }

    @queue = :update_state

    @retry_limit = 6000
    @retry_delay = 20

    def self.perform(method, *args)
      send(method, *args)
    end

    def self.start(rfid, position, cpf, role='cyclist', invalid=false)
      station_number = CompartbikeApi.server_config[:station_number]
      self.create(rfid: rfid, position: position, cpf: cpf, station_number: station_number, role: role, invalid: invalid)
    end
  end
end
