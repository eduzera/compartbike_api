require 'resque'

module CompartbikeApi
  class Slot
    include Mongoid::Document
    include Mongoid::Timestamps

    has_many :releases, class_name: 'CompartbikeApi::Release'
    has_many :rentals, class_name: 'CompartbikeApi::Rental'

    field :name, type: Integer
    field :bike, type: String
    field :sensor_status, type: String

    validates :name, presence: true

    state_machine :syncronization, initial: :not_syncronized do
      event :not_updated do
        transition any => :not_syncronized
      end

      event :updated do
        transition :not_syncronized => :syncronized
      end

      state :not_syncronized
      state :syncronized
    end

    state_machine initial: :off do
      event :fill do
        transition [:off, :empty, :disabled, :disconnected] => :busy
      end

      event :clean do
        transition [:off, :empty, :releasing, :disabled, :disconnected] => :empty
      end

      event :release do
        transition [:disabled, :busy] => :releasing
      end

      event :rent do
        transition [:releasing] => :empty
      end

      event :cancel_rent do
        transition [:releasing] => :busy
      end

      event :error do
        transition any => :disabled
      end

      event :reset do
        transition any => :off
      end

      event :disconnect do
        transition any => :disconnected
      end

      state :off
      state :empty
      state :busy
      state :releasing
      state :disabled
      state :disconnected

      after_transition any                        => any,         :do => :set_not_syncronized
      after_transition :off                       => [:disabled], :do => :off_to_disable
      after_transition :off                       => :busy,       :do => :off_to_busy
      after_transition :empty                     => [:disabled], :do => :empty_to_disable
      after_transition :empty                     => :busy,       :do => :empty_to_busy
      after_transition :busy                      => [:disabled], :do => :busy_to_disable
      after_transition :busy                      => :releasing,  :do => :busy_to_releasing
      after_transition :releasing                 => :busy,       :do => :releasing_to_busy
      after_transition :releasing                 => :empty,      :do => :releasing_to_empty
      after_transition :releasing                 => [:disabled], :do => :releasing_to_disabled
      after_transition [:disabled, :disconnected] => :empty,      :do => :disable_to_empty
      after_transition [:disabled, :disconnected] => :busy,       :do => :disable_to_busy
      after_transition :disabled                  => :releasing,  :do => :disable_to_releasing
      after_transition any                        => :off,        :do => :any_to_off
    end

    def as_json(options={})
      super(only: [:name, :state, :bike, :sensor_status, :syncronization])
    end

    private
      # --- Transitions
      def set_not_syncronized
        not_updated!
      end

      def any_to_off
        Resque.pole_position(CompartbikeBoard::Commands, :reset, name)
        buz
      end

      def off_to_disable
        set_board_errors
      end

      def off_to_busy
        turn_off_red_led
        notify_bike_received
      end

      def empty_to_busy
        notify_bike_received
        flash_buz_and_led_green
      end

      def empty_to_disable
        set_board_errors
      end

      def busy_to_disable
        set_board_errors
      end

      def busy_to_releasing
        set_release_bike
      end

      def releasing_to_busy
        rental = Rental.where(slot: self, rfid: bike).last
        Resque.enqueue(CompartbikeApi::RemoteRental, :delete, rental.remote_id) if rental.present?

        flash_buz_and_led_red
      end

      def releasing_to_empty
        flash_buz_and_led_green
      end

      def releasing_to_disabled
        if bike
          rental = Rental.where(slot: self, rfid: bike).last
          Resque.enqueue(CompartbikeApi::RemoteRental, :delete, rental.remote_id)
        end
        set_board_errors
      end

      def disable_to_empty
        turn_off_red_led
      end

      def disable_to_busy
        turn_off_red_led
        flash_buz_and_led_green
        notify_bike_received
      end

      def disable_to_releasing
        set_release_bike
      end

      # --- Notifications
      def notify_bike_received
        Resque.enqueue(CompartbikeApi::RemoteSlot, :notify_bike_received, name, bike, Time.now)
      end

      # --- Methods
      def returned_bike
        if self.bike
          notify_bike_received
          flash_buz_and_led_green
        end
      end

      # --- Enqueue Commands
      def flash_buz_and_led_green
        Resque.pole_position(CompartbikeBoard::Commands, :flash, :buz, 2, name)
        Resque.pole_position(CompartbikeBoard::Commands, :flash, :led_green, 2, name)
      end

      def set_board_errors
        Resque.pole_position(CompartbikeBoard::Commands, :turn_on, :buz, name, 2)
        Resque.pole_position(CompartbikeBoard::Commands, :turn_on, :led_red, name)
      end

      def flash_buz_and_led_red
        buz
        Resque.pole_position(CompartbikeBoard::Commands, :flash, :led_red, 2, name)
      end

      def set_release_bike
        Resque.pole_position(CompartbikeBoard::Commands, :release_bike, name)
      end

      def buz(times=2)
        Resque.pole_position(CompartbikeBoard::Commands, :flash, :buz, times, name)
      end

      def turn_off_red_led
        Resque.pole_position(CompartbikeBoard::Commands, :turn_off, :led_red, name)
      end
  end
end
