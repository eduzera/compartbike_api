require "time_diff"

module CompartbikeApi
  class SlotObserver
    attr_accessor :slot
    @queue = :update_state

    def self.perform(slot_name)
      slot_observer = new(Slot.where(name: slot_name).first)
      slot_observer.update_state
    end

    def initialize slot
      @slot = slot
    end

    def update_state
      off_status          if :off       == @slot.state_name
      clean_status        if :empty     == @slot.state_name
      busy_status         if :busy      == @slot.state_name
      releasing_status    if :releasing == @slot.state_name
      error_status        if :disabled  == @slot.state_name

      notify_server_changed_state if @slot.not_syncronized?
    end

    private
      def off_status
        @slot.error! if there_is_error_on_sensor
        @slot.fill!  if there_is_bike_in_both_sensors
        @slot.clean! if there_isnt_bike_in_both_sensors
      end

      def clean_status
        @slot.error! if there_is_error_on_sensor
        @slot.fill!  if there_is_bike_in_both_sensors
      end

      def busy_status
        @slot.error! if there_is_error_on_sensor
        @slot.error! if there_isnt_bike_in_both_sensors && @slot.bike
      end

      def error_status
        @slot.fill!  if there_is_bike_in_both_sensors
        @slot.clean! if there_isnt_bike_in_both_sensors
      end

      def releasing_status
        @slot.cancel_rent!  if release_expired? && there_is_bike_in_both_sensors
        @slot.rent!         if there_isnt_bike_in_both_sensors
        @slot.error!        if there_is_error_on_sensor && release_expired?
      end

      def there_is_bike_in_both_sensors
       !!(@slot.bike and @slot.sensor_status == "has_bike")
      end

      def there_is_error_on_sensor
        @slot.sensor_status == "error" ||
          (@slot.sensor_status == "has_not_bike" && @slot.bike.present?) ||
          (@slot.sensor_status == "has_bike" && @slot.bike.blank?)
      end

      def there_isnt_bike_in_both_sensors
        @slot.bike.nil? and @slot.sensor_status == "has_not_bike"
      end

      def error_without_bike
        @slot.bike.nil? && @slot.sensor_status == "error"
      end

      def release_expired?
        diff = Time.diff(Time.now, @slot.updated_at)

        if diff[:minute] >= 1
          true
        else
          false
        end
      end

      def notify_server_changed_state
        CompartbikeApi::RemoteSlot.update_slot_status_on_server(@slot.name, @slot.state, @slot.bike)
      end
  end
end
