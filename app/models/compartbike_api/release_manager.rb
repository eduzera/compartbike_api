module CompartbikeApi
  class ReleaseManager
    attr_accessor :slot, :cpf, :rental, :role

    def initialize slot, cpf, role='cyclist'
      @slot            = slot
      @cpf             = cpf
      @role            = role
      @rental
    end

    def create_release
      return false if @slot.disabled?
      return false unless @slot.can_release?

      return create_release_for_cyclist if @role == 'cyclist'
      return create_release_for_mechanical if @role == 'mechanical'
    end

    def create_release_for_cyclist
      @rental = CompartbikeApi::RemoteRental.start(@slot.bike, @slot.name, @cpf, @role)

      if @rental.valid? 
        @slot.release
        Rental.create(remote_id: @rental.id, rfid: @slot.bike, slot: @slot, cpf: @cpf, role: 'cyclist')
      else
        return false
      end
    rescue Errno::ECONNREFUSED
      false
    end

    def create_release_for_mechanical
      Resque.enqueue(CompartbikeApi::RemoteRental, :start, @slot.bike, @slot.name, @cpf, @role)
      @slot.release
      Rental.create(rfid: @slot.bike, slot: @slot, cpf: @cpf, role: 'mechanical')
      return true
    end
    
    def create_invalid_rent
       @rental = CompartbikeApi::RemoteRental.start(@slot.bike, @slot.name, @cpf, @role, true)
    end
  end
end
