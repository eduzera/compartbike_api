module CompartbikeApi
  class SlotBoardObserver
    attr_accessor :slot
    @queue = :commands

    def self.perform(slot_name)
      slot_board = new(Slot.where(name: slot_name).first)
      slot_board.check_board_status
    end

    def initialize(slot)
      @slot = slot
    end

    def check_board_status
      response = CompartbikeBoard::Commands.check_status(@slot.name)
      @slot.update_attributes(bike: response.bike, sensor_status: response.sensor_status)
      @slot.reset! if @slot.disconnected?
    rescue CompartbikeBoard::NoResponseError
      @slot.disconnect! if 10.seconds.ago.utc > @slot.updated_at
    end
  end
end
