module CompartbikeApi
  class SlotManager

    def self.find(slot_name)
      right_name = select_slot(slot_name)
      CompartbikeApi::Slot.where(name: right_name).first
    end

    private

    def self.select_slot(slot_name)
      skip_slots = [10, 17, 19]

      right_name = skip_slots.inject(slot_name.to_i) do |number, slot|
        number += 1 if number >= slot
        number
      end

      right_name.to_s
    end
  end
end
