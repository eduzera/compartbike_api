module CompartbikeApi
  class Release
    include Mongoid::Document
    include Mongoid::Timestamps

    default_scope desc(:created_at)

    belongs_to :slot, class_name: 'CompartbikeApi::Slot'

    field :cyclist    , type: String
    field :from_mechanical , type: Boolean, default: false
    field :bike       , type: String

    scope :status_releasing  , where(state: 'releasing') 
    scope :status_released   , where(state: 'released') 
    scope :status_error_sync , where(state: 'not_syncronized') 

    state_machine :initial => :releasing do
      event :success do
        transition :releasing => :released
      end
      event :failure do
        transition :releasing => :canceled
      end
      event :error_on_sync do
        transition :released => :not_syncronized
      end
      event :success_on_sync do
        transition :released => :syncronized
      end
      state :releasing
      state :canceled
      state :released
      state :syncronized
      state :not_syncronized
    end

    def as_json(options={})
      super(only: [:state, :cyclist, :bike], methods: :formated_created_at)
    end

    def formated_created_at
      I18n.l created_at, format: :short
    end

  end
end