module CompartbikeApi
  class CyclistManager
    
    def initialize(cpf)
      @user = CompartbikeApi::RemoteCyclist.find(cpf)

    rescue ActiveResource::ResourceNotFound
    end

    def json
      return not_found      unless @user
      return station_closed unless RemoteStation.working?

      @json_answer = {
                        cpf: @user.cpf,
                        login: @user.login,
                        type: @user.type,
                        valid: true
                     }

      @json_answer.merge!(error_message) unless valid?
      @json_answer
    end

    private

    def not_found
      {
        valid: false,
        reason: 'not_found',
        message: I18n.t('messages.not_found')
      }
    end

    def station_closed
      {
        valid: false,
        reason: 'station_closed',
        message: I18n.t('messages.station_closed')
      }
    end

    def valid?
      @user.show_status == 'valid' && @user.can_rent_more_bikes && @user.valid_ticket && @user.valid_ticket
    end

    def error_message
      errors = {valid: false}

      if !@user.valid_ticket
        errors.merge({
          reason: 'ticket_expired',
          message: I18n.t("messages.ticket_expired")
        })
      elsif @user.can_rent_more_bikes
        errors.merge({
          reason: @user.show_status,
          message: I18n.t("messages.#{@user.show_status}")
        })
      else
        if newest_rental?
          errors.merge({
            reason: 'waiting_rent',
            message: I18n.t('messages.waiting_rent')
          })
        else
          errors.merge({
            reason: 'cannot_rent',
            message: I18n.t('messages.cannot_rent')
          })
        end
      end
    end

    def newest_rental?
      rental = Rental.where(cpf: @user.cpf).last
      rental && rental.created_at > 60.seconds.ago
    end
  end
end