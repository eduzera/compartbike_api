module CompartbikeApi
  class Rental
    include Mongoid::Document
    include Mongoid::Timestamps

    belongs_to :slot, class_name: 'CompartbikeApi::Slot'
    field :cpf, type: String
    field :rfid, type: String
    field :remote_id, type: String
    field :role, type: String
  end
end
