module CompartbikeApi
  class RemoteCyclist < ActiveResource::Base
    self.site         = CompartbikeApi.server_config[:path] + '/api/v1'
    self.user         = CompartbikeApi.server_config[:login]
    self.password     = CompartbikeApi.server_config[:password]
    self.format       = :json
    self.element_name = :cyclist
  end
end