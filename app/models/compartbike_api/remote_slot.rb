require 'resque-retry'

module CompartbikeApi
  class RemoteSlot < ActiveResource::Base
    extend Resque::Plugins::Retry

    self.site         = CompartbikeApi.server_config[:path]
    self.format       = :json
    self.user         = CompartbikeApi.server_config[:login]
    self.password     = CompartbikeApi.server_config[:password]
    self.element_name = :slot

    schema { string :position, :status, :station_id, :bike_id }

    @queue = :update_state

    @retry_limit = 3000
    @retry_delay = 60

    def self.generate_slots
      return false if Rails.env.test?

      all.each do |remote_slot|
        Slot.create name: remote_slot.position
      end
    end

    def self.perform(method, *args)
      send(method, *args)
    end

    def self.notify_bike_received(name, bike, timestamp)
      Rails.logger.info " RemoteSlot.notify_bike_received calling: #{CompartbikeApi.server_config[:station_number]} ,with params: position => #{name}, rfid: #{bike}, received_at: #{timestamp}"

      CompartbikeApi::RemoteSlot.post(:bike_received, station_number: CompartbikeApi.server_config[:station_number], rfid: bike, position: name, received_at: timestamp)
    end

    def self.update_slot_status_on_server(name, state, bike)
      slot = CompartbikeApi::Slot.where(name: name).first

      begin
        Rails.logger.info " RemoteSlot.update_slot_status_on_server calling: #{CompartbikeApi.server_config[:station_number]} ,with params: position => #{name}, status: #{state}, rfid: #{bike}"
        CompartbikeApi::RemoteSlot.put(:update_from_api, station_number: CompartbikeApi.server_config[:station_number], position: name, status: state, rfid: bike)
        slot.updated!
      rescue Exception
        slot.not_updated!
      end
    end

    def self.notify_bike_released(bike, name, cyclist)
      slot = CompartbikeApi::Slot.where(name: name).first
      release = slot.releases.status_released.first

      begin
        Rails.logger.info " RemoteSlot.notify_bike_released calling: #{CompartbikeApi.server_config[:station_number]} ,with params: position => #{name}, cpf: #{cyclist}, rfid: #{bike}"
        CompartbikeApi::RemoteSlot.post(:bike_released, station_number: CompartbikeApi.server_config[:station_number], position: name, rfid: bike, cpf: cyclist)
        release.success_on_sync!
      rescue Exception
        release.error_on_sync!
      end
    end
  end
end
