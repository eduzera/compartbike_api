require 'resque'

namespace :slots do
  desc "check all slots status on board"
  task :check do
    slots_size = CompartbikeApi::Slot.all.size
    while true do
      if Resque.size(:commands) < slots_size
        CompartbikeApi::Slot.all.each do |slot|
          puts "checking slot #{slot.name}..."
          Resque.enqueue(CompartbikeApi::SlotBoardObserver, slot.name)
        end
      end
      sleep(1)
    end
  end

  desc "update all slots status"
  task :update do
    slots_size = CompartbikeApi::Slot.all.size
    while true do
      if Resque.size(:update_state) < slots_size
        CompartbikeApi::Slot.all.each do |slot|
          puts "updating slot #{slot.name}..."
          Resque.enqueue(CompartbikeApi::SlotObserver, slot.name)
        end
      end
      sleep(1)
    end
  end
end