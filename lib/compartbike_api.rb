require "compartbike_api/engine"

module CompartbikeApi
  class << self
    attr_accessor :server_config
  end

  def self.config
    config = YAML.load_file("#{Rails.root}/config/compartbike_api.yml")[Rails::env]
    config.symbolize_keys!

    self.server_config = {
      path:           config[:server]['path'],
      login:          config[:server]['login'],
      password:       config[:server]['password'],
      station_number: config[:server]['station_number'],
      board_mounted:  config[:board]['mount']
    }
  end
end
