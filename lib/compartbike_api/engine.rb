module CompartbikeApi
  require 'compartbike_board'
  require 'state_machine'

  class Engine < ::Rails::Engine
    isolate_namespace CompartbikeApi

    config.generators do |g|
      g.test_framework :rspec
      g.integration_tool :rspec
      g.stylesheets false
      g.javascripts false
    end

    initializer 'create board_connection' do |app|
      begin
        if Rails.env.production?
          device = CompartbikeApi.config.fetch(:board_mounted, '/dev/ttyS0')
          CompartbikeBoard::Board.new(device, 9600).connect!
        else
          CompartbikeBoard::Board.new('/dev/tty.usbserial', 9600).connect!
        end
      rescue
        puts 'nao inicializou plaquinha...'
      end
    end

    initializer 'config slots' do |app|
      #total slots on station
      CompartbikeApi.config
    end
  end
end
