CompartbikeApi::Engine.routes.draw do

  resources :slots
  post  'slots/:id/release'         => 'slots#release'
  get   'slots/:id/reset'           => 'slots#reset'
  get   'slots/:id'                 => 'slots#show'
  get   'releases'                  => 'releases#index'
  get   'releases/not_syncronized'  => 'releases#not_syncronized'
  get   'cyclists/:cpf'   => 'cyclists#show'
  get   'mechanicals/:cpf'   => 'mechanicals#show'
end
