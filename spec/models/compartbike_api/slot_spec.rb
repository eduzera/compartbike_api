require 'spec_helper'

describe CompartbikeApi::Slot do
  it { should have_fields(:name) }

  it { should have_many(:rentals) }
  it { should validate_presence_of(:name) }

  before { CompartbikeApi::Slot.destroy_all }

  describe 'state machine' do
    describe 'any to disconnect' do
      context 'after 10 seconds of losing connection' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: nil, sensor_status: :has_not_bike, updated_at: 11.seconds.ago }
        before { CompartbikeBoard::Commands.stub(:check_status).and_raise(CompartbikeBoard::NoResponseError) }

        it 'should transit state to disconnected' do
          CompartbikeApi::SlotBoardObserver.perform(slot.name)
          slot.reload.state_name.should == :disconnected
        end

        it 'should receive disconnect!' do
          CompartbikeApi::Slot.any_instance.should_receive(:disconnect!)
          CompartbikeApi::SlotBoardObserver.perform(slot.name)
        end
      end

      context 'before 10 seconds of losing connection' do
        let(:updated_at) {(1..9).to_a.sample.seconds.ago}
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: nil, sensor_status: :has_not_bike, updated_at: updated_at }
        before { CompartbikeBoard::Commands.stub(:check_status).and_raise(CompartbikeBoard::NoResponseError) }

        it 'should not transit state to disconnected' do
          CompartbikeApi::SlotBoardObserver.perform(slot.name)
          slot.reload.state_name.should == :off
        end

        it 'should receive disconnect!' do
          CompartbikeApi::Slot.any_instance.should_not_receive(:disconnect!)
          CompartbikeApi::SlotBoardObserver.perform(slot.name)
        end
      end
    end

    describe 'disconnect to off' do
      context 'reestablished connection' do
        let(:slot) do
          CompartbikeApi::Slot.create(name: 1, bike: nil, sensor_status: :has_not_bike, state: 'disconnected')
        end

        before do
          set_board_status(nil, :has_not_bike)
        end

        it 'should transit state to off' do
          CompartbikeApi::SlotBoardObserver.perform(slot.name)
          slot.reload.state_name.should == :off
        end

        it 'should receive reset!' do
          CompartbikeApi::Slot.any_instance.should_receive(:reset!)
          CompartbikeApi::SlotBoardObserver.perform(slot.name)
        end
      end
    end

    describe 'STATE off' do
      context 'to fill' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: 'AC00033A5F', sensor_status: :has_bike }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :has_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "has_bike" }
          it { slot.reload.bike.should == 'AC00033A5F' }
        end

        it "should transit state to busy" do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :busy
        end

        it "should receive fill!" do
          CompartbikeApi::Slot.any_instance.should_receive(:fill!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to fill with errors' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: 'AC00033A5F', sensor_status: :error }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :error)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "error" }
          it { slot.reload.bike.should == 'AC00033A5F' }
        end

        it "should transit state to disabled", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :disabled
        end

        it "should receive error!" do
          CompartbikeApi::Slot.any_instance.should_receive(:error!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to clean' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: nil, sensor_status: :has_not_bike }

        context "checking board" do
          before do
            set_board_status(nil, :has_not_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "has_not_bike" }
          it { slot.reload.bike.should be_nil }
        end

        it "should transit state to empty" do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :empty
        end

        it "should receive clean!" do
          CompartbikeApi::Slot.any_instance.should_receive(:clean!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'when reset' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: nil, sensor_status: 'has_not_bike' }

        it "should transit to off state" do
          Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :reset, 1)
          Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :flash, :buz, 2, 1)

          slot.reset!

          slot.reload.state_name.should == :off
        end
      end
    end

    describe 'STATE clean' do
      context 'to fill' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, state: "empty", bike: 'AC00033A5F', sensor_status: "has_bike" }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :has_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "has_bike" }
          it { slot.reload.bike.should == 'AC00033A5F' }
        end

        it "should transit state to busy" do
          timestamp = 5.minutes.ago
          Time.stub(now: timestamp)
          CompartbikeApi::RemoteSlot.should_receive(:update_slot_status_on_server).with(1, "busy", slot.bike)
          Resque.should_receive(:enqueue).with(CompartbikeApi::RemoteSlot, :notify_bike_received, 1, "AC00033A5F", timestamp)
          Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :flash, :buz, 2, 1)
          Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :flash, :led_green, 2, 1)

          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :busy
        end

        it "should receive fill!" do
          CompartbikeApi::Slot.any_instance.should_receive(:fill!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end

      end

      context 'to error with bike' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, state: "empty", bike: 'AC00033A5F', sensor_status: "error" }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :error)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.sensor_status.should == "error" }
          it { slot.bike.should == 'AC00033A5F' }
        end

        it "should transit state to disabled", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :disabled
        end

        it "should receive error!" do
          CompartbikeApi::Slot.any_instance.should_receive(:error!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to error with rfid' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, state: "empty", bike: 'AC00033A5F', sensor_status: "has_not_bike" }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :has_not_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.sensor_status.should == "has_not_bike" }
          it { slot.bike.should == 'AC00033A5F' }
        end

        it "should transit state to disabled", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :disabled
        end

        it "should receive error!" do
          CompartbikeApi::Slot.any_instance.should_receive(:error!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to error with bike and without rfid' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, state: "empty", bike: nil, sensor_status: "has_bike" }

        context "checking board" do
          before do
            set_board_status(nil, :has_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.sensor_status.should == "has_bike" }
          it { slot.bike.should == nil }
        end

        it "should transit state to disabled", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :disabled
        end

        it "should receive error!" do
          CompartbikeApi::Slot.any_instance.should_receive(:error!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to error without bike' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, state: "empty", bike: nil, sensor_status: "error" }

        context "checking board" do
          before do
            set_board_status(nil, :error)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.sensor_status.should == "error" }
          it { slot.bike.should == nil }
        end

        it "should transit state to disabled", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :disabled
        end

        it "should receive error!" do
          CompartbikeApi::Slot.any_instance.should_receive(:error!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end
    end

    describe 'STATE busy' do
      context 'to releasing', vcr: true do
        let!(:slot) { CompartbikeApi::Slot.create name: 1, state: 'busy', bike: 'AC00033A5F', sensor_status: "has_bike" }

        before do
          CompartbikeApi::Release.create(slot: slot, cyclist: cpf, bike: 'AC00033A5F')
        end

        context 'with successful release' do
          let(:cpf) { '352.777.458-07' }

          it "should transit state to releasing" do
            Resque.should_receive(:pole_position)
            .with(CompartbikeBoard::Commands, :release_bike, slot.name)

            slot.release!
            slot.reload.state_name.should == :releasing
          end
        end
      end

      context 'to error with bike' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, state: "busy", bike: 'AC00033A5F', sensor_status: "error" }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :error)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "error" }
          it { slot.reload.bike.should == 'AC00033A5F' }
        end

        it "should transit state to disabled", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :disabled
        end

        it "should receive error!" do
          CompartbikeApi::Slot.any_instance.should_receive(:error!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end
    end

    describe 'STATE disabled' do
      context 'to busy' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: 'AC00033A5F', sensor_status: "has_bike", state: "disabled" }

        context "checking board" do
          before do
            set_board_status('AC00033A5F', :has_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "has_bike" }
          it { slot.reload.bike.should == 'AC00033A5F' }
        end

        it "should receive turn_off_red_led" do
          CompartbikeApi::Slot.any_instance.should_receive(:turn_off_red_led)
          CompartbikeApi::Slot.any_instance.should_receive(:flash_buz_and_led_green)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end

        it "should transit state to busy", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :busy
        end

        it "should receive fill!" do
          CompartbikeApi::Slot.any_instance.should_receive(:fill!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to empty' do
        let(:slot) { CompartbikeApi::Slot.create name: 1, bike: nil, sensor_status: "has_not_bike", state: "disabled" }
        context "checking board" do
          before do
            set_board_status(nil, :has_not_bike)
            CompartbikeApi::SlotBoardObserver.perform(slot.name)
          end

          it { slot.reload.sensor_status.should == "has_not_bike" }
          it { slot.reload.bike.should be_nil }
        end

        it "should receive turn_off_red_led" do
          CompartbikeApi::Slot.any_instance.should_receive(:turn_off_red_led)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end

        it "should transit state to empty", depends_redis: true do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :empty
        end

        it "should receive clean!" do
          CompartbikeApi::Slot.any_instance.should_receive(:clean!)
          CompartbikeApi::SlotObserver.perform(slot.name)
        end
      end

      context 'to releasing', vcr: true do
        let!(:slot) { CompartbikeApi::Slot.create name: 1, state: 'disabled', bike: nil, sensor_status: "has_not_bike" }

        context 'with successful release' do
          it "should transit state to releasing" do
            Resque.should_receive(:pole_position)
            .with(CompartbikeBoard::Commands, :release_bike, slot.name)

            slot.release!
            slot.reload.state_name.should == :releasing
          end
        end
      end
    end

    describe 'STATE releasing' do
      context 'to empty' do
        let!(:release) { CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: 'AC00033A5F') }
        let!(:slot) { CompartbikeApi::Slot.create(name: 1, state: "releasing", bike: nil, sensor_status: "has_not_bike") }

        before do
          CompartbikeApi::RemoteSlot.should_receive(:update_slot_status_on_server).with(1, "empty", slot.bike)
          Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :flash, :buz, 2, slot.name)
          Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :flash, :led_green, 2, slot.name)
        end

        it "should transit to state empty" do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.should be_empty
        end
      end

      context 'when still releasing' do
        let!(:slot) { CompartbikeApi::Slot.create(name: 1, state: "releasing", bike: "AC00033A5F", sensor_status: "has_bike") }

        it 'should not change state' do
          Delorean.time_travel_to 30.seconds.from_now

          CompartbikeApi::SlotObserver.perform(slot.name)

          slot.reload.state_name.should == :releasing
        end
      end

      context 'when expired time and bike still at slot' do
        let!(:slot) { CompartbikeApi::Slot.create(name: 1, state: "releasing", bike: "AC00033A5F", sensor_status: "has_bike") }
        let!(:rental) { CompartbikeApi::Rental.create(rfid: "AC00033A5F", slot: slot, remote_id: 3) }

        def expire_rental
          Delorean.time_travel_to 3.minutes.from_now
          CompartbikeApi::SlotObserver.perform(slot.name)
        end

        it "shoud return to busy" do
          expire_rental
          slot.reload.state_name.should == :busy
        end

        it "should call perform on RemoteRental" do
          Resque.should_receive(:enqueue).with(CompartbikeApi::RemoteRental, :delete, "3")
          expire_rental
        end
      end

      context 'without error on sensor' do
        let!(:slot) { CompartbikeApi::Slot.create(name: 1, state: "releasing", bike: "AC00033A5F", sensor_status: "error") }
        let!(:rental) { CompartbikeApi::Rental.create(rfid: "AC00033A5F", slot: slot, remote_id: 3) }

        def expire_time
          Delorean.time_travel_to 3.minutes.from_now
          CompartbikeApi::SlotObserver.perform(slot.name)
        end

        it "shoud return error" do
          expire_time
          slot.reload.state_name.should == :disabled
        end

        it "should call perform on RemoteRental" do
          Resque.should_receive(:enqueue).with(CompartbikeApi::RemoteRental, :delete, "3")
          expire_time
        end
      end

      context 'without a rfid' do
        let!(:slot) { CompartbikeApi::Slot.create(name: 1, state: "releasing", bike: nil, sensor_status: "error") }
        let!(:rental) { CompartbikeApi::Rental.create(rfid: "AC00033A5F", slot: slot, remote_id: 3) }

        def expire_time
          Delorean.time_travel_to 3.minutes.from_now
          CompartbikeApi::SlotObserver.perform(slot.name)
        end

        it 'should not change state' do
          CompartbikeApi::SlotObserver.perform(slot.name)
          slot.reload.state_name.should == :releasing
        end

        context 'after 1 minute' do
          it 'shoud return error' do
            expire_time
            slot.reload.state_name.should == :disabled
          end

          it 'should set board errors' do
            Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :turn_on, :buz, slot.name, 2)
            Resque.should_receive(:pole_position).with(CompartbikeBoard::Commands, :turn_on, :led_red, slot.name)
            expire_time
          end
        end

        it "should call perform on RemoteRental" do
          Resque.should_not_receive(:enqueue).with(CompartbikeApi::RemoteRental, "3")
          expire_time
        end
      end

    end
  end

  describe "#check_status" do
    let(:slot) { CompartbikeApi::Slot.create name: 1 }

    it "should fill slot's sensor_status with data", depends_redis: true do
      slot.sensor_status.should be_blank
      set_board_status(nil, :has_not_bike)

      CompartbikeApi::SlotBoardObserver.perform(slot.name)
      slot.reload.sensor_status.should be_present
    end
  end
end

def set_board_status(bike=nil, sensor_status=:error)
  CompartbikeBoard::Commands.stub(check_status: double("response", bike: bike, sensor_status: sensor_status))
end
