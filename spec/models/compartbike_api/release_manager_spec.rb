require 'spec_helper'

describe CompartbikeApi::ReleaseManager do
  context 'cyclist' do
    describe '#create_release', vcr: true do
      let!(:slot){ CompartbikeApi::Slot.create name: 1, bike: 'AC00033A5F', state: slot_state }

      context 'successful release' do
        let(:slot_state){ 'busy' }
        let(:release_manager){ described_class.new(slot, '327.944.136-60') }
        before{ slot.stub(release: true) }

        it 'should create a rental' do
          expect{ release_manager.create_release }.to change{ CompartbikeApi::Rental.count }.by(1)
        end

        it 'should return true' do
          release_manager.create_release.should be_true
        end
      end

      context 'unsuccessful release' do
        context 'disabled' do
          let(:slot_state) {'disabled'}
          let(:release_manager){ described_class.new(slot, '327.944.136-60') }

          before{ CompartbikeApi::RemoteRental.should_not_receive(:start) }

          it 'should not create a rental' do
            expect{ release_manager.create_release }.to_not change{ CompartbikeApi::Rental.count }
          end

          it 'should return false' do
            release_manager.create_release.should be_false
          end
        end

        context 'another invalid state' do
          let(:slot_state) {%w(off empty releasing disabled).sample}
          let(:release_manager){ described_class.new(slot, '327.944.136-60') }

          before{ CompartbikeApi::RemoteRental.should_not_receive(:start) }

          it 'should not create a rental' do
            expect{ release_manager.create_release }.to_not change{ CompartbikeApi::Rental.count }
          end

          it 'should return false' do
            release_manager.create_release.should be_false
          end
        end
      end

      context 'with connection error' do
        let(:slot_state){ 'busy' }
        let(:release_manager){ described_class.new(slot, '327.944.136-60') }
        
        before do
          CompartbikeApi::RemoteRental.stub(:start){ raise Errno::ECONNREFUSED }
        end

        it 'should not create a rental' do
          expect{ release_manager.create_release }.to_not change{ CompartbikeApi::Rental.count }
        end

        it 'should return false' do
          release_manager.create_release.should be_false
        end
      end
    end
  end

  context 'mechanical' do
    describe '#create_release', vcr: true do
      let!(:slot){ CompartbikeApi::Slot.create name: 1, bike: 'AC00033A5F', state: slot_state }

      context 'successful release' do
        context 'busy' do
          let(:slot_state){ 'busy' }
          let(:release_manager){ described_class.new(slot, '516.315.851-18', 'mechanical') }
          before{ slot.stub(release: true) }

          it 'should create a rental' do
            expect{ release_manager.create_release }.to change{ CompartbikeApi::Rental.count }.by(1)
          end

          it 'should return true' do
            release_manager.create_release.should be_true
          end
        end

        context 'disabled' do
          let(:slot_state){ 'disabled' }
          let(:release_manager){ described_class.new(slot, '516.315.851-18', 'mechanical') }
          before{ slot.stub(release: true) }

          it 'should not create a rental' do
            expect{ release_manager.create_release }.to_not change{ CompartbikeApi::Rental.count }.by(1)
          end

          it 'should not return true' do
            release_manager.create_release.should_not be_true
          end
        end
      end
    end
  end
end
