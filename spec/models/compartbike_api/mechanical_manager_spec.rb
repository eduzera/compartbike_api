#encoding: utf-8

require "spec_helper"

describe CompartbikeApi::MechanicalManager do
  describe "mechanical" do
    context 'when login is valid' do
      let(:status) {'valid'}
      let(:can_rent) {true}
      let(:attributes) {{cpf: '352.777.458-07', login: 'Eduardo', type: 'Mechanical'}}
      let(:mechanical) {double(attributes)}

      before {CompartbikeApi::RemoteMechanical.should_receive(:find).with(mechanical.login){ mechanical }}
      subject {CompartbikeApi::MechanicalManager.new(mechanical.login).json}

      it{ subject[:valid].should be_true }
      it{ subject[:type].should eq('Mechanical') }
      it{ subject[:cpf].should eq(mechanical.cpf) }
      it{ subject[:login].should eq(mechanical.login) }
    end
  
    context 'when login is invalid' do
      let(:mechanical) {double(login: '35277745807')}

      before do
        CompartbikeApi::RemoteMechanical
          .should_receive(:find)
          .with(mechanical.login)
          .and_raise(ActiveResource::ResourceNotFound.new(404, 'Not Found'))
      end

      subject {CompartbikeApi::MechanicalManager.new(mechanical.login).json}

      it{ subject[:valid].should be_false }
      it{ subject[:reason].should eq('not_found') }
    end
  end
end