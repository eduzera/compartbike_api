require 'spec_helper'

describe CompartbikeApi::SlotManager do
  before do
    CompartbikeApi::Release.destroy_all
    CompartbikeApi::Slot.destroy_all
  end

  describe '.find' do
    let!(:slot) do
      CompartbikeApi::Slot
        .create(name: slot_name,
                bike: 'AC00033A5F',
                state: 'busy')
    end

    subject { described_class.find(slot_param) }

    context 'with a normal slot name' do
      let(:slot_name) { 1 }
      let(:slot_param) { 1 }

      it { should eq(slot) }
    end

    context 'with especial slot' do
      context 'name 10' do
        let(:slot_name) { 11 }
        let(:slot_param) { 10 }

        it { should eq(slot) }
      end

      context 'name 16' do
        let(:slot_name) { 18 }
        let(:slot_param) { 16 }

        it { should eq(slot) }
      end

      context 'name 18' do
        let(:slot_name) { 21 }
        let(:slot_param) { 18 }

        it { should eq(slot) }
      end
    end
  end
end
