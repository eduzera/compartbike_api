require 'spec_helper'

describe CompartbikeApi::RemoteSlot do
  before do 
    CompartbikeApi::Release.destroy_all
    CompartbikeApi::Slot.destroy_all
  end

  let!(:slot)   { CompartbikeApi::Slot.create(name: 1, state: "empty", bike: nil, sensor_status: "has_not_bike")}
  let!(:release){ CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: 'AC00033A5F', state: 'released')}

  describe "notify_byke_released" do
    context 'when have access internet' do
      before{ CompartbikeApi::RemoteSlot.stub(:post).and_return(true) }

      it 'should have release with state success_sincronyzed' do
        CompartbikeApi::RemoteSlot.perform(:notify_bike_released,'AC00033A5F', '1', '352.777.458-07')
        release.reload.state_name.should == :syncronized
      end
    end
    context 'when not have access internet' do
      before{ CompartbikeApi::RemoteSlot.stub(:post).and_raise(Exception.new) }

      it 'should have release with state not_sincronyzed' do |variable|
        CompartbikeApi::RemoteSlot.perform(:notify_bike_released,'AC00033A5F', '1', '352.777.458-07')
        release.reload.state_name.should == :not_syncronized
      end
    end
  end

  describe "update_slot_status_on_server" do
    let!(:slot)   { CompartbikeApi::Slot.create(name: 1, state: "empty", bike: nil, sensor_status: "has_not_bike", syncronization: 'not_syncronized')}

    context 'when can syncronize status with server' do
      before{ CompartbikeApi::RemoteSlot.stub(:put).and_return(true) }

      it "slot should be syncronized" do
        CompartbikeApi::RemoteSlot.perform(:update_slot_status_on_server, slot.name, slot.state, slot.bike)
        slot.reload.should be_syncronized
      end
    end
    context 'when can NOT syncronize status with server' do
      before{ CompartbikeApi::RemoteSlot.stub(:put).and_raise(Exception.new)}

      it "slot should be syncronized" do
        CompartbikeApi::RemoteSlot.perform(:update_slot_status_on_server, slot.name, slot.state, slot.bike)
        slot.reload.should be_not_syncronized
      end
    end
  end
end
