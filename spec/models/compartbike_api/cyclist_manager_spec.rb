#encoding: utf-8

require "spec_helper"

describe CompartbikeApi::CyclistManager do
  describe "cyclist" do
    context 'when login is valid' do
      let(:status) {'valid'}
      let(:can_rent) {true}
      let(:attributes) {{cpf: '352.777.458-07', login: '352.777.458-07', type: 'Cyclist', show_status: status, can_rent_more_bikes: can_rent}}
      let(:cyclist) {double(attributes)}

      before do
        CompartbikeApi::RemoteCyclist.should_receive(:find).with(cyclist.login){ cyclist }
        CompartbikeApi::RemoteStation.stub(:working?){ true }
      end

      subject {CompartbikeApi::CyclistManager.new(cyclist.login).json}

      context 'when station is closed' do
        before{ CompartbikeApi::RemoteStation.stub(:working?){ false }}

        it{ subject[:valid].should be_false }
        it{ subject[:reason].should eq('station_closed') }
        it{ subject[:message].should eq('Estação Fechada') }
      end

      context 'when cyclist is valid' do
        it{ subject[:valid].should be_true }
        it{ subject[:type].should eq('Cyclist') }
        it{ subject[:cpf].should eq(cyclist.cpf) }
        it{ subject[:login].should eq(cyclist.login) }
      end

      context 'when cyclist is invalid' do
        context 'when not paid' do
          let(:status) {'not_paid'}
 
          it{ subject[:valid].should be_false }
          it{ subject[:type].should eq('Cyclist') }
          it{ subject[:cpf].should eq(cyclist.cpf) }
          it{ subject[:login].should eq(cyclist.login) }
          it{ subject[:reason].should eq('not_paid') }
          it{ subject[:message].should eq('É necessário pagar a taxa de cadastro para poder retirar bicicletas.')}
        end

        context 'when blocked' do
          let(:status) {'blocked'}
          
          it{ subject[:valid].should be_false }
          it{ subject[:type].should eq('Cyclist') }
          it{ subject[:cpf].should eq(cyclist.cpf) }
          it{ subject[:login].should eq(cyclist.login) }
          it{ subject[:reason].should eq('blocked') }
          it{ subject[:message].should eq('Conta bloqueada. Contate um funcionário.')}
        end

        context 'when blocked_by_admin' do
          let(:status) {'blocked_by_admin'}
          
          it{ subject[:valid].should be_false }
          it{ subject[:type].should eq('Cyclist') }
          it{ subject[:cpf].should eq(cyclist.cpf) }
          it{ subject[:login].should eq(cyclist.login) }
          it{ subject[:reason].should eq('blocked_by_admin') }
          it{ subject[:message].should eq('Conta bloqueada. Contate um funcionário.')}
        end

        context 'when creditcard_expired' do
          let(:status) {'creditcard_expired'}
          
          it{ subject[:valid].should be_false }
          it{ subject[:type].should eq('Cyclist') }
          it{ subject[:cpf].should eq(cyclist.cpf) }
          it{ subject[:login].should eq(cyclist.login) }
          it{ subject[:reason].should eq('creditcard_expired') }
          it{ subject[:message].should eq('O cartão de crédito cadastrado expirou. Por favor cadastre um novo cartão no sistema.')}
        end

        context 'when cannot rent more bike' do
          let(:can_rent) {false}
          let(:slot) {CompartbikeApi::Slot.create(name: 1, state: "releasing", bike: "AC00033A5F", sensor_status: "has_bike")}

          context 'with a rental' do
            context 'an old rental' do
              before do
                Timecop.travel(61.seconds.ago) do
                  CompartbikeApi::Rental.create(cpf: attributes[:cpf], rfid: "AC00033A5F", slot: slot, remote_id: 3)
                end
              end

              it{ subject[:valid].should be_false }
              it{ subject[:type].should eq('Cyclist') }
              it{ subject[:cpf].should eq(cyclist.cpf) }
              it{ subject[:login].should eq(cyclist.login) }
              it{ subject[:reason].should eq('cannot_rent') }
              it{ subject[:message].should eq('Você já ultrapassou o limite de empréstimos. Devolva uma bicicleta para poder voltar a usar o sistema.')}
            end

            context 'a newest rental' do
              before {CompartbikeApi::Rental.create(cpf: attributes[:cpf], rfid: "AC00033A5F", slot: slot, remote_id: 3)}

              it{ subject[:valid].should be_false }
              it{ subject[:type].should eq('Cyclist') }
              it{ subject[:cpf].should eq(cyclist.cpf) }
              it{ subject[:login].should eq(cyclist.login) }
              it{ subject[:reason].should eq('waiting_rent') }
              it{ subject[:message].should eq('Última demanda de empréstimo em tratamento. Aguarde 1 minuto.')}
            end
          end

          context 'without a rental' do
            it{ subject[:valid].should be_false }
            it{ subject[:type].should eq('Cyclist') }
            it{ subject[:cpf].should eq(cyclist.cpf) }
            it{ subject[:login].should eq(cyclist.login) }
            it{ subject[:reason].should eq('cannot_rent') }
            it{ subject[:message].should eq('Você já ultrapassou o limite de empréstimos. Devolva uma bicicleta para poder voltar a usar o sistema.')}
          end
        end
      end
    end
  
    context 'when login is invalid' do
      let(:cyclist) {double(login: '35277745807')}

      before do
        CompartbikeApi::RemoteCyclist
          .should_receive(:find)
          .with(cyclist.login)
          .and_raise(ActiveResource::ResourceNotFound.new(404, 'Not Found'))
      end

      subject {CompartbikeApi::CyclistManager.new(cyclist.login).json}

      it{ subject[:valid].should be_false }
      it{ subject[:reason].should eq('not_found') }
      it{ subject[:message].should eq('Usuário não encontrado. Tente novamente!')}
    end
  end
end