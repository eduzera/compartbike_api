require "spec_helper"

describe CompartbikeApi::RemoteRental do
  describe "::perform" do
    it "should call delete" do
      CompartbikeApi::RemoteRental.should_receive(:delete).with(3)
      CompartbikeApi::RemoteRental.perform(:delete, 3)
    end

    it "should call start" do
      CompartbikeApi::RemoteRental.should_receive(:start).with("123", "1", "33943325822", "cyclist")
      CompartbikeApi::RemoteRental.perform(:start, "123", "1", "33943325822", "cyclist")
    end
  end
end
