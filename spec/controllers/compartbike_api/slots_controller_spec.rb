require 'spec_helper'

describe CompartbikeApi::SlotsController do
  before{ CompartbikeApi::Slot.destroy_all }

  describe 'index' do
    let!(:slot){ CompartbikeApi::Slot.create name: 1 }

    it 'should list slots' do
      get :index, :format => 'json'

      response.body.should == [slot].to_json
    end
  end

  describe 'show' do
    let!(:slot){ CompartbikeApi::Slot.create name: 1 }

    it 'list just one slot' do
      get :show, id: 1, format: 'json'
      response.body.should == slot.to_json
    end
  end

  describe 'release' do
    let(:release_manager){ double(:release_manager, create_release: true)}
    before{ CompartbikeApi::ReleaseManager.stub(new: release_manager) }

    it 'calls create_release on release_manager' do
      release_manager.should_receive(:create_release)
      put :release, id: 1
    end

    context 'unsuccessful' do
      let(:release_manager){ double(:release_manager, create_release: false)}

      it 'returns 500' do
        put :release, id: 1
        response.code.should == "500"
      end
    end

    context 'successful' do
      it 'returns 200' do
        put :release, id: 1
        response.code.should == "200"
      end
    end
  end

  describe 'reset' do
    let!(:slot){ CompartbikeApi::Slot.create name: 1, bike: 'AC00033A5F', state: 'busy' }

    before do
      CompartbikeApi::SlotManager
        .should_receive(:find)
        .with("1")
        .and_return(slot)

      get :reset, id: 1
    end

    it "should reset slot by name" do
      response.body.should == "Reset Command sent to slot 1"
    end
  end
end
