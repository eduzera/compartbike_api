require 'spec_helper'

describe CompartbikeApi::CyclistsController do
  describe 'GET show' do
    let(:manager) {double('CyclistManager')}
    let(:cpf) {'12345678901'}

    before do
      CompartbikeApi::CyclistManager.should_receive(:new).with(cpf) {manager}
      manager.should_receive(:json)
    end

    it 'show the cyclist informations' do
      get :show, cpf: cpf
      response.status.should == 200
    end
  end
end