require 'spec_helper'

describe CompartbikeApi::MechanicalsController do
  describe 'GET show' do
    let(:manager) {double('MechanicalManager')}
    let(:cpf) {'12345678901'}

    before do
      CompartbikeApi::MechanicalManager.should_receive(:new).with(cpf) {manager}
      manager.should_receive(:json)
    end

    it 'show the mechanical informations' do
      get :show, cpf: cpf
      response.status.should == 200
    end
  end
end