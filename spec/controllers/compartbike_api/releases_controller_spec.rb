require 'spec_helper'

describe CompartbikeApi::ReleasesController do
  before do 
    CompartbikeApi::Slot.destroy_all 
    CompartbikeApi::Release.destroy_all 
  end

  let (:slot)           {CompartbikeApi::Slot.create(name: 1)}
  let!(:releasing)      { CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: '01', state: 'releasing')}
  let!(:canceled)       { CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: '02', state: 'canceled')}
  let!(:released)       { CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: '03', state: 'released')}
  let!(:syncronized)    { CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: '04', state: 'syncronized')}
  let!(:not_syncronized){ CompartbikeApi::Release.create(slot: slot, cyclist: '352.777.458-07', bike: '05', state: 'not_syncronized')}

  describe 'GET index' do
    before {get :index, format: 'json'}
    subject {response.body}

    it {should be_include(releasing.to_json)}
    it {should be_include(canceled.to_json)}
    it {should be_include(released.to_json)}
    it {should be_include(syncronized.to_json)}
    it {should be_include(not_syncronized.to_json)}
  end

  describe 'GET release/not_syncronized' do
    it 'should list all releases not syncronized' do
      get :not_syncronized, format: 'json'
      response.body.should == [not_syncronized].to_json
    end
  end
end